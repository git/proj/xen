From: Eric Sandeen <sandeen@redhat.com>
Date: Wed, 17 Oct 2007 06:27:15 +0000 (-0700)
Subject: minixfs: limit minixfs printks on corrupted dir i_size (CVE-2006-6058)
X-Git-Tag: v2.6.23.7~3
X-Git-Url: http://git.kernel.org/?p=linux%2Fkernel%2Fgit%2Fstable%2Flinux-2.6.23.y.git;a=commitdiff_plain;h=f0ae3188daf70ed07a4dfbeb133bef3a92838a15

minixfs: limit minixfs printks on corrupted dir i_size (CVE-2006-6058)

patch f44ec6f3f89889a469773b1fd894f8fcc07c29cf upstream.

This attempts to address CVE-2006-6058
http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-6058

first reported at http://projects.info-pull.com/mokb/MOKB-17-11-2006.html

Essentially a corrupted minix dir inode reporting a very large
i_size will loop for a very long time in minix_readdir, minix_find_entry,
etc, because on EIO they just move on to try the next page.  This is
under the BKL, printk-storming as well.  This can lock up the machine
for a very long time.  Simply ratelimiting the printks gets things back
under control.  Make the message a bit more informative while we're here.

Signed-off-by: Eric Sandeen <sandeen@redhat.com>
Cc: Bodo Eggert <7eggert@gmx.de>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
Signed-off-by: Linus Torvalds <torvalds@linux-foundation.org>
Signed-off-by: Greg Kroah-Hartman <gregkh@suse.de>
---

Backported to Debian's 2.6.18 by dann frazier <dannf@debian.org>

diff -urpN linux-source-2.6.18.orig/fs/minix/itree_v1.c linux-source-2.6.18/fs/minix/itree_v1.c
--- linux-source-2.6.18.orig/fs/minix/itree_v1.c	2006-09-19 21:42:06.000000000 -0600
+++ linux-source-2.6.18/fs/minix/itree_v1.c	2007-12-16 19:13:41.000000000 -0700
@@ -23,11 +23,16 @@ static inline block_t *i_data(struct ino
 static int block_to_path(struct inode * inode, long block, int offsets[DEPTH])
 {
 	int n = 0;
+	char b[BDEVNAME_SIZE];
 
 	if (block < 0) {
-		printk("minix_bmap: block<0\n");
+		printk("MINIX-fs: block_to_path: block %ld < 0 on dev %s\n",
+			block, bdevname(inode->i_sb->s_bdev, b));
 	} else if (block >= (minix_sb(inode->i_sb)->s_max_size/BLOCK_SIZE)) {
-		printk("minix_bmap: block>big\n");
+		if (printk_ratelimit())
+			printk("MINIX-fs: block_to_path: "
+			       "block %ld too big on dev %s\n",
+				block, bdevname(inode->i_sb->s_bdev, b));
 	} else if (block < 7) {
 		offsets[n++] = block;
 	} else if ((block -= 7) < 512) {
diff -urpN linux-source-2.6.18.orig/fs/minix/itree_v2.c linux-source-2.6.18/fs/minix/itree_v2.c
--- linux-source-2.6.18.orig/fs/minix/itree_v2.c	2006-09-19 21:42:06.000000000 -0600
+++ linux-source-2.6.18/fs/minix/itree_v2.c	2007-12-16 19:40:06.000000000 -0700
@@ -23,11 +23,17 @@ static inline block_t *i_data(struct ino
 static int block_to_path(struct inode * inode, long block, int offsets[DEPTH])
 {
 	int n = 0;
+	char b[BDEVNAME_SIZE];
+	struct super_block *sb = inode->i_sb;
 
 	if (block < 0) {
-		printk("minix_bmap: block<0\n");
+		printk("MINIX-fs: block_to_path: block %ld < 0 on dev %s\n",
+			block, bdevname(sb->s_bdev, b));
 	} else if (block >= (minix_sb(inode->i_sb)->s_max_size/BLOCK_SIZE)) {
-		printk("minix_bmap: block>big\n");
+		if (printk_ratelimit())
+			printk("MINIX-fs: block_to_path: "
+			       "block %ld too big on dev %s\n",
+				block, bdevname(sb->s_bdev, b));
 	} else if (block < 7) {
 		offsets[n++] = block;
 	} else if ((block -= 7) < 256) {
