Subject: xen3 xen-kconfig
From: http://xenbits.xensource.com/xen-3.1-testing.hg (tip 15042)
Patch-mainline: obsolete
Acked-by: jbeulich@novell.com

Index: head-2007-09-25/arch/i386/Kconfig
===================================================================
--- head-2007-09-25.orig/arch/i386/Kconfig	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/arch/i386/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -16,6 +16,7 @@ config X86_32
 
 config GENERIC_TIME
 	bool
+	depends on !X86_XEN
 	default y
 
 config CLOCKSOURCE_WATCHDOG
@@ -131,6 +132,15 @@ config X86_PC
 	help
 	  Choose this option if your computer is a standard PC or compatible.
 
+config X86_XEN
+	bool "Xen-compatible"
+	select X86_UP_APIC if !SMP && XEN_PRIVILEGED_GUEST
+	select X86_UP_IOAPIC if !SMP && XEN_PRIVILEGED_GUEST
+	select SWIOTLB
+	help
+	  Choose this option if you plan to run this kernel on top of the
+	  Xen Hypervisor.
+
 config X86_ELAN
 	bool "AMD Elan"
 	help
@@ -261,6 +271,7 @@ source "arch/i386/Kconfig.cpu"
 
 config HPET_TIMER
 	bool "HPET Timer Support"
+	depends on !X86_XEN
 	help
 	  This enables the use of the HPET for the kernel's internal timer.
 	  HPET is the next generation timer replacing legacy 8254s.
@@ -311,7 +322,7 @@ source "kernel/Kconfig.preempt"
 
 config X86_UP_APIC
 	bool "Local APIC support on uniprocessors"
-	depends on !SMP && !(X86_VISWS || X86_VOYAGER || X86_GENERICARCH)
+	depends on !SMP && !(X86_VISWS || X86_VOYAGER || X86_GENERICARCH || XEN_UNPRIVILEGED_GUEST)
 	help
 	  A local APIC (Advanced Programmable Interrupt Controller) is an
 	  integrated interrupt controller in the CPU. If you have a single-CPU
@@ -336,12 +347,12 @@ config X86_UP_IOAPIC
 
 config X86_LOCAL_APIC
 	bool
-	depends on X86_UP_APIC || ((X86_VISWS || SMP) && !X86_VOYAGER) || X86_GENERICARCH
+	depends on X86_UP_APIC || ((X86_VISWS || SMP) && !(X86_VOYAGER || XEN_UNPRIVILEGED_GUEST)) || X86_GENERICARCH
 	default y
 
 config X86_IO_APIC
 	bool
-	depends on X86_UP_IOAPIC || (SMP && !(X86_VISWS || X86_VOYAGER)) || X86_GENERICARCH
+	depends on X86_UP_IOAPIC || (SMP && !(X86_VISWS || X86_VOYAGER || XEN_UNPRIVILEGED_GUEST)) || X86_GENERICARCH
 	default y
 
 config X86_VISWS_APIC
@@ -351,7 +362,7 @@ config X86_VISWS_APIC
 
 config X86_MCE
 	bool "Machine Check Exception"
-	depends on !X86_VOYAGER
+	depends on !(X86_VOYAGER || X86_XEN)
 	---help---
 	  Machine Check Exception support allows the processor to notify the
 	  kernel if it detects a problem (e.g. overheating, component failure).
@@ -450,6 +461,7 @@ config X86_REBOOTFIXUPS
 
 config MICROCODE
 	tristate "/dev/cpu/microcode - Intel IA32 CPU microcode support"
+	depends on !XEN_UNPRIVILEGED_GUEST
 	select FW_LOADER
 	---help---
 	  If you say Y here and also to "/dev file system support" in the
@@ -473,6 +485,7 @@ config MICROCODE_OLD_INTERFACE
 
 config X86_MSR
 	tristate "/dev/cpu/*/msr - Model-specific register support"
+	depends on !X86_XEN
 	help
 	  This device gives privileged processes access to the x86
 	  Model-Specific Registers (MSRs).  It is a character device with
@@ -488,6 +501,10 @@ config X86_CPUID
 	  with major 203 and minors 0 to 31 for /dev/cpu/0/cpuid to
 	  /dev/cpu/31/cpuid.
 
+config SWIOTLB
+	bool
+	default n
+
 source "drivers/firmware/Kconfig"
 
 choice
@@ -674,6 +691,7 @@ config HIGHPTE
 
 config MATH_EMULATION
 	bool "Math emulation"
+	depends on !X86_XEN
 	---help---
 	  Linux can emulate a math coprocessor (used for floating point
 	  operations) if you don't have one. 486DX and Pentium processors have
@@ -699,6 +717,8 @@ config MATH_EMULATION
 
 config MTRR
 	bool "MTRR (Memory Type Range Register) support"
+	depends on !XEN_UNPRIVILEGED_GUEST
+	default y if X86_XEN
 	---help---
 	  On Intel P6 family processors (Pentium Pro, Pentium II and later)
 	  the Memory Type Range Registers (MTRRs) may be used to control
@@ -733,7 +753,7 @@ config MTRR
 
 config EFI
 	bool "Boot from EFI support"
-	depends on ACPI
+	depends on ACPI && !X86_XEN
 	default n
 	---help---
 	This enables the kernel to boot on EFI platforms using
@@ -751,7 +771,7 @@ config EFI
 
 config IRQBALANCE
  	bool "Enable kernel irq balancing"
-	depends on SMP && X86_IO_APIC
+	depends on SMP && X86_IO_APIC && !X86_XEN
 	default y
 	help
  	  The default yes will allow the kernel to do irq load balancing.
@@ -785,6 +805,7 @@ source kernel/Kconfig.hz
 
 config KEXEC
 	bool "kexec system call"
+	depends on !XEN_UNPRIVILEGED_GUEST
 	help
 	  kexec is a system call that implements the ability to shutdown your
 	  current kernel, and to start another kernel.  It is like a reboot
@@ -900,6 +921,7 @@ config HOTPLUG_CPU
 
 config COMPAT_VDSO
 	bool "Compat VDSO support"
+	depends on !X86_XEN
 	default y
 	help
 	  Map the VDSO to the predictable old-style address too.
@@ -917,15 +939,17 @@ config ARCH_ENABLE_MEMORY_HOTPLUG
 	depends on HIGHMEM
 
 menu "Power management options (ACPI, APM)"
-	depends on !X86_VOYAGER
+	depends on !(X86_VOYAGER || XEN_UNPRIVILEGED_GUEST)
 
+if !X86_XEN
 source kernel/power/Kconfig
+endif
 
 source "drivers/acpi/Kconfig"
 
 menuconfig APM
 	tristate "APM (Advanced Power Management) BIOS support"
-	depends on PM && !X86_VISWS
+	depends on PM && !(X86_VISWS || X86_XEN)
 	---help---
 	  APM is a BIOS specification for saving power using several different
 	  techniques. This is mostly useful for battery powered laptops with
@@ -1094,6 +1118,7 @@ choice
 
 config PCI_GOBIOS
 	bool "BIOS"
+	depends on !X86_XEN
 
 config PCI_GOMMCONFIG
 	bool "MMConfig"
@@ -1101,6 +1126,13 @@ config PCI_GOMMCONFIG
 config PCI_GODIRECT
 	bool "Direct"
 
+config PCI_GOXEN_FE
+	bool "Xen PCI Frontend"
+	depends on X86_XEN
+	help
+	  The PCI device frontend driver allows the kernel to import arbitrary
+	  PCI devices from a PCI backend to support PCI driver domains.
+
 config PCI_GOANY
 	bool "Any"
 
@@ -1108,7 +1140,7 @@ endchoice
 
 config PCI_BIOS
 	bool
-	depends on !X86_VISWS && PCI && (PCI_GOBIOS || PCI_GOANY)
+	depends on !(X86_VISWS || X86_XEN) && PCI && (PCI_GOBIOS || PCI_GOANY)
 	default y
 
 config PCI_DIRECT
@@ -1121,6 +1153,18 @@ config PCI_MMCONFIG
 	depends on PCI && ACPI && (PCI_GOMMCONFIG || PCI_GOANY)
 	default y
 
+config XEN_PCIDEV_FRONTEND
+	bool
+	depends on PCI && X86_XEN && (PCI_GOXEN_FE || PCI_GOANY)
+	default y
+
+config XEN_PCIDEV_FE_DEBUG
+	bool "Xen PCI Frontend Debugging"
+	depends on XEN_PCIDEV_FRONTEND
+	default n
+	help
+	  Enables some debug statements within the PCI Frontend.
+
 source "drivers/pci/pcie/Kconfig"
 
 source "drivers/pci/Kconfig"
@@ -1131,7 +1175,7 @@ config ISA_DMA_API
 
 config ISA
 	bool "ISA support"
-	depends on !(X86_VOYAGER || X86_VISWS)
+	depends on !(X86_VOYAGER || X86_VISWS || X86_XEN)
 	help
 	  Find out whether you have ISA slots on your motherboard.  ISA is the
 	  name of a bus system, i.e. the way the CPU talks to the other stuff
@@ -1158,7 +1202,7 @@ config EISA
 source "drivers/eisa/Kconfig"
 
 config MCA
-	bool "MCA support" if !(X86_VISWS || X86_VOYAGER)
+	bool "MCA support" if !(X86_VISWS || X86_VOYAGER || X86_XEN)
 	default y if X86_VOYAGER
 	help
 	  MicroChannel Architecture is found in some IBM PS/2 machines and
@@ -1234,6 +1278,8 @@ source "security/Kconfig"
 
 source "crypto/Kconfig"
 
+source "drivers/xen/Kconfig"
+
 source "lib/Kconfig"
 
 #
@@ -1259,7 +1305,7 @@ config X86_SMP
 
 config X86_HT
 	bool
-	depends on SMP && !(X86_VISWS || X86_VOYAGER)
+	depends on SMP && !(X86_VISWS || X86_VOYAGER || X86_XEN)
 	default y
 
 config X86_BIOS_REBOOT
@@ -1272,6 +1318,16 @@ config X86_TRAMPOLINE
 	depends on X86_SMP || (X86_VOYAGER && SMP)
 	default y
 
+config X86_NO_TSS
+	bool
+	depends on X86_XEN
+	default y
+
+config X86_NO_IDT
+	bool
+	depends on X86_XEN
+	default y
+
 config KTIME_SCALAR
 	bool
 	default y
Index: head-2007-09-25/arch/i386/Kconfig.cpu
===================================================================
--- head-2007-09-25.orig/arch/i386/Kconfig.cpu	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/arch/i386/Kconfig.cpu	2007-09-25 14:34:51.000000000 +0200
@@ -274,7 +274,7 @@ config X86_PPRO_FENCE
 
 config X86_F00F_BUG
 	bool
-	depends on M586MMX || M586TSC || M586 || M486 || M386
+	depends on (M586MMX || M586TSC || M586 || M486 || M386) && !X86_NO_IDT
 	default y
 
 config X86_WP_WORKS_OK
@@ -334,7 +334,7 @@ config X86_OOSTORE
 
 config X86_TSC
 	bool
-	depends on (MWINCHIP3D || MWINCHIP2 || MCRUSOE || MEFFICEON || MCYRIXIII || MK7 || MK6 || MPENTIUM4 || MPENTIUMM || MPENTIUMIII || MPENTIUMII || M686 || M586MMX || M586TSC || MK8 || MVIAC3_2 || MVIAC7 || MGEODEGX1 || MGEODE_LX || MCORE2) && !X86_NUMAQ
+	depends on (MWINCHIP3D || MWINCHIP2 || MCRUSOE || MEFFICEON || MCYRIXIII || MK7 || MK6 || MPENTIUM4 || MPENTIUMM || MPENTIUMIII || MPENTIUMII || M686 || M586MMX || M586TSC || MK8 || MVIAC3_2 || MVIAC7 || MGEODEGX1 || MGEODE_LX || MCORE2) && !X86_NUMAQ && !X86_XEN
 	default y
 
 # this should be set for all -march=.. options where the compiler
Index: head-2007-09-25/arch/i386/Kconfig.debug
===================================================================
--- head-2007-09-25.orig/arch/i386/Kconfig.debug	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/arch/i386/Kconfig.debug	2007-09-25 14:34:51.000000000 +0200
@@ -79,6 +79,7 @@ config X86_MPPARSE
 config DOUBLEFAULT
 	default y
 	bool "Enable doublefault exception handler" if EMBEDDED
+	depends on !X86_NO_TSS
 	help
           This option allows trapping of rare doublefault exceptions that
           would otherwise cause a system to silently reboot. Disabling this
Index: head-2007-09-25/arch/x86_64/Kconfig
===================================================================
--- head-2007-09-25.orig/arch/x86_64/Kconfig	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/arch/x86_64/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -183,6 +183,22 @@ config GENERIC_CPU
 
 endchoice
 
+config X86_64_XEN
+	bool "Enable Xen compatible kernel"
+	select SWIOTLB
+	help
+	  This option will compile a kernel compatible with Xen hypervisor
+
+config X86_NO_TSS
+	bool
+	depends on X86_64_XEN
+	default y
+
+config X86_NO_IDT
+	bool
+	depends on X86_64_XEN
+	default y
+
 #
 # Define implied options from the CPU selection here
 #
@@ -203,6 +219,7 @@ config X86_INTERNODE_CACHE_BYTES
 
 config X86_TSC
 	bool
+	depends on !X86_64_XEN
 	default y
 
 config X86_GOOD_APIC
@@ -251,7 +268,7 @@ config X86_CPUID
 
 config X86_HT
 	bool
-	depends on SMP && !MK8
+	depends on SMP && !MK8 && !X86_64_XEN
 	default y
 
 config MATH_EMULATION
@@ -265,14 +282,22 @@ config EISA
 
 config X86_IO_APIC
 	bool
+	depends !XEN_UNPRIVILEGED_GUEST
 	default y
 
+config X86_XEN_GENAPIC
+	bool
+	depends X86_64_XEN
+	default XEN_PRIVILEGED_GUEST || SMP
+
 config X86_LOCAL_APIC
 	bool
+	depends !XEN_UNPRIVILEGED_GUEST
 	default y
 
 config MTRR
 	bool "MTRR (Memory Type Range Register) support"
+	depends on !XEN_UNPRIVILEGED_GUEST
 	---help---
 	  On Intel P6 family processors (Pentium Pro, Pentium II and later)
 	  the Memory Type Range Registers (MTRRs) may be used to control
@@ -313,7 +338,7 @@ config SMP
 
 config SCHED_SMT
 	bool "SMT (Hyperthreading) scheduler support"
-	depends on SMP
+	depends on SMP && !X86_64_XEN
 	default n
 	help
 	  SMT scheduler support improves the CPU scheduler's decision making
@@ -323,7 +348,7 @@ config SCHED_SMT
 
 config SCHED_MC
 	bool "Multi-core scheduler support"
-	depends on SMP
+	depends on SMP && !X86_64_XEN
 	default y
 	help
 	  Multi-core scheduler support improves the CPU scheduler's decision
@@ -334,7 +359,7 @@ source "kernel/Kconfig.preempt"
 
 config NUMA
        bool "Non Uniform Memory Access (NUMA) Support"
-       depends on SMP
+       depends on SMP && !X86_64_XEN
        help
 	 Enable NUMA (Non Uniform Memory Access) support. The kernel 
 	 will try to allocate memory used by a CPU on the local memory 
@@ -390,7 +415,7 @@ config ARCH_DISCONTIGMEM_DEFAULT
 
 config ARCH_SPARSEMEM_ENABLE
 	def_bool y
-	depends on (NUMA || EXPERIMENTAL)
+	depends on (NUMA || EXPERIMENTAL) && !X86_64_XEN
 
 config ARCH_MEMORY_PROBE
 	def_bool y
@@ -418,6 +443,7 @@ config NR_CPUS
 	int "Maximum number of CPUs (2-255)"
 	range 2 255
 	depends on SMP
+	default "16" if X86_64_XEN
 	default "8"
 	help
 	  This allows you to specify the maximum number of CPUs which this
@@ -443,6 +469,7 @@ config ARCH_ENABLE_MEMORY_HOTPLUG
 
 config HPET_TIMER
 	bool
+	depends on !X86_64_XEN
 	default y
 	help
 	  Use the IA-PC HPET (High Precision Event Timer) to manage
@@ -463,7 +490,7 @@ config IOMMU
 	default y
 	select SWIOTLB
 	select AGP
-	depends on PCI
+	depends on PCI && !X86_64_XEN
 	help
 	  Support for full DMA access of devices with 32bit memory access only
 	  on systems with more than 3GB. This is usually needed for USB,
@@ -478,7 +505,7 @@ config IOMMU
 config CALGARY_IOMMU
 	bool "IBM Calgary IOMMU support"
 	select SWIOTLB
-	depends on PCI && EXPERIMENTAL
+	depends on PCI && !X86_64_XEN && EXPERIMENTAL
 	help
 	  Support for hardware IOMMUs in IBM's xSeries x366 and x460
 	  systems. Needed to run systems with more than 3GB of memory
@@ -516,6 +543,7 @@ config SWIOTLB
 
 config X86_MCE
 	bool "Machine check support" if EMBEDDED
+	depends on !X86_64_XEN
 	default y
 	help
 	   Include a machine check error handler to report hardware errors.
@@ -541,6 +569,7 @@ config X86_MCE_AMD
 
 config KEXEC
 	bool "kexec system call"
+	depends on !XEN_UNPRIVILEGED_GUEST
 	help
 	  kexec is a system call that implements the ability to shutdown your
 	  current kernel, and to start another kernel.  It is like a reboot
@@ -691,8 +720,11 @@ config GENERIC_PENDING_IRQ
 	default y
 
 menu "Power management options"
+	depends on !XEN_UNPRIVILEGED_GUEST
 
+if !X86_64_XEN
 source kernel/power/Kconfig
+endif
 
 source "drivers/acpi/Kconfig"
 
@@ -716,6 +748,21 @@ config PCI_MMCONFIG
 	bool "Support mmconfig PCI config space access"
 	depends on PCI && ACPI
 
+config XEN_PCIDEV_FRONTEND
+	bool "Xen PCI Frontend"
+	depends on PCI && X86_64_XEN
+	default y
+	help
+	  The PCI device frontend driver allows the kernel to import arbitrary
+	  PCI devices from a PCI backend to support PCI driver domains.
+
+config XEN_PCIDEV_FE_DEBUG
+	bool "Xen PCI Frontend Debugging"
+	depends on XEN_PCIDEV_FRONTEND
+	default n
+	help
+	  Enables some debug statements within the PCI Frontend.
+
 source "drivers/pci/pcie/Kconfig"
 
 source "drivers/pci/Kconfig"
@@ -786,4 +833,6 @@ source "security/Kconfig"
 
 source "crypto/Kconfig"
 
+source "drivers/xen/Kconfig"
+
 source "lib/Kconfig"
Index: head-2007-09-25/drivers/acpi/Kconfig
===================================================================
--- head-2007-09-25.orig/drivers/acpi/Kconfig	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/drivers/acpi/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -48,7 +48,7 @@ if ACPI
 
 config ACPI_SLEEP
 	bool "Sleep States"
-	depends on X86 && (!SMP || SUSPEND_SMP)
+	depends on X86 && (!SMP || SUSPEND_SMP) && !XEN
 	depends on PM
 	default y
 	---help---
@@ -327,6 +327,7 @@ config ACPI_SYSTEM
 config X86_PM_TIMER
 	bool "Power Management Timer Support" if EMBEDDED
 	depends on X86
+	depends on !XEN
 	default y
 	help
 	  The Power Management Timer is available on all ACPI-capable,
Index: head-2007-09-25/drivers/char/tpm/Kconfig
===================================================================
--- head-2007-09-25.orig/drivers/char/tpm/Kconfig	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/drivers/char/tpm/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -59,5 +59,13 @@ config TCG_INFINEON
 	  Further information on this driver and the supported hardware
 	  can be found at http://www.prosec.rub.de/tpm
 
-endmenu
+config TCG_XEN
+	tristate "XEN TPM Interface"
+	depends on TCG_TPM && XEN
+	---help---
+	  If you want to make TPM support available to a Xen user domain,
+	  say Yes and it will be accessible from within Linux.
+	  To compile this driver as a module, choose M here; the module
+	  will be called tpm_xenu.
 
+endmenu
Index: head-2007-09-25/drivers/firmware/Kconfig
===================================================================
--- head-2007-09-25.orig/drivers/firmware/Kconfig	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/drivers/firmware/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -8,6 +8,7 @@ menu "Firmware Drivers"
 config EDD
 	tristate "BIOS Enhanced Disk Drive calls determine boot disk"
 	depends on !IA64
+	depends on !XEN
 	help
 	  Say Y or M here if you want to enable BIOS Enhanced Disk Drive
 	  Services real mode BIOS calls to determine which disk
Index: head-2007-09-25/drivers/serial/Kconfig
===================================================================
--- head-2007-09-25.orig/drivers/serial/Kconfig	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/drivers/serial/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -12,6 +12,7 @@ menu "Serial drivers"
 config SERIAL_8250
 	tristate "8250/16550 and compatible serial support"
 	depends on (BROKEN || !SPARC)
+	depends on !XEN_DISABLE_SERIAL
 	select SERIAL_CORE
 	---help---
 	  This selects whether you want to include the driver for the standard
Index: head-2007-09-25/drivers/video/console/Kconfig
===================================================================
--- head-2007-09-25.orig/drivers/video/console/Kconfig	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/drivers/video/console/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -46,6 +46,7 @@ config VGACON_SOFT_SCROLLBACK_SIZE
 config VIDEO_SELECT
 	bool "Video mode selection support"
 	depends on  X86 && VGA_CONSOLE
+	depends on !XEN
 	---help---
 	  This enables support for text mode selection on kernel startup. If
 	  you want to take advantage of some high-resolution text mode your
Index: head-2007-09-25/drivers/xen/Kconfig
===================================================================
--- /dev/null	1970-01-01 00:00:00.000000000 +0000
+++ head-2007-09-25/drivers/xen/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -0,0 +1,260 @@
+#
+# This Kconfig describe xen options
+#
+
+mainmenu "Xen Configuration"
+
+config XEN
+	bool
+	default y if X86_XEN || X86_64_XEN
+	help
+	  This is the Linux Xen port.
+
+if XEN
+config XEN_INTERFACE_VERSION
+	hex
+	default 0x00030205
+
+menu "XEN"
+
+config XEN_PRIVILEGED_GUEST
+	bool "Privileged Guest (domain 0)"
+	depends XEN
+	default n
+	help
+	  Support for privileged operation (domain 0)
+
+config XEN_UNPRIVILEGED_GUEST
+	bool
+	default !XEN_PRIVILEGED_GUEST
+
+config XEN_PRIVCMD
+	bool
+	depends on PROC_FS
+	default y
+
+config XEN_XENBUS_DEV
+	bool
+	depends on PROC_FS
+	default y
+
+config XEN_BACKEND
+        tristate "Backend driver support"
+        default y
+        help
+          Support for backend device drivers that provide I/O services
+          to other virtual machines.
+
+config XEN_BLKDEV_BACKEND
+	tristate "Block-device backend driver"
+        depends on XEN_BACKEND
+	default y
+	help
+	  The block-device backend driver allows the kernel to export its
+	  block devices to other guests via a high-performance shared-memory
+	  interface.
+
+config XEN_BLKDEV_TAP
+	tristate "Block-device tap backend driver"
+	depends on XEN_BACKEND
+	default XEN_PRIVILEGED_GUEST
+	help
+	  The block tap driver is an alternative to the block back driver 
+          and allows VM block requests to be redirected to userspace through
+          a device interface.  The tap allows user-space development of 
+          high-performance block backends, where disk images may be implemented
+          as files, in memory, or on other hosts across the network.  This 
+	  driver can safely coexist with the existing blockback driver.
+
+config XEN_NETDEV_BACKEND
+	tristate "Network-device backend driver"
+        depends on XEN_BACKEND && NET
+	default y
+	help
+	  The network-device backend driver allows the kernel to export its
+	  network devices to other guests via a high-performance shared-memory
+	  interface.
+
+config XEN_NETDEV_PIPELINED_TRANSMITTER
+	bool "Pipelined transmitter (DANGEROUS)"
+	depends on XEN_NETDEV_BACKEND
+	default n
+	help
+	  If the net backend is a dumb domain, such as a transparent Ethernet
+	  bridge with no local IP interface, it is safe to say Y here to get
+	  slightly lower network overhead.
+	  If the backend has a local IP interface; or may be doing smart things
+	  like reassembling packets to perform firewall filtering; or if you
+	  are unsure; or if you experience network hangs when this option is
+	  enabled; then you must say N here.
+
+config XEN_NETDEV_LOOPBACK
+	tristate "Network-device loopback driver"
+	depends on XEN_NETDEV_BACKEND
+	default y
+	help
+	  A two-interface loopback device to emulate a local netfront-netback
+	  connection.
+
+config XEN_PCIDEV_BACKEND
+	tristate "PCI-device backend driver"
+	depends on PCI && XEN_BACKEND
+	default XEN_PRIVILEGED_GUEST
+	help
+	  The PCI device backend driver allows the kernel to export arbitrary
+	  PCI devices to other guests. If you select this to be a module, you
+	  will need to make sure no other driver has bound to the device(s)
+	  you want to make visible to other guests.
+
+choice
+	prompt "PCI Backend Mode"
+	depends on XEN_PCIDEV_BACKEND
+	default XEN_PCIDEV_BACKEND_VPCI
+
+config XEN_PCIDEV_BACKEND_VPCI
+	bool "Virtual PCI"
+	---help---
+	  This PCI Backend hides the true PCI topology and makes the frontend
+	  think there is a single PCI bus with only the exported devices on it.
+	  For example, a device at 03:05.0 will be re-assigned to 00:00.0. A
+	  second device at 02:1a.1 will be re-assigned to 00:01.1.
+
+config XEN_PCIDEV_BACKEND_PASS
+	bool "Passthrough"
+	---help---
+	  This PCI Backend provides a real view of the PCI topology to the
+	  frontend (for example, a device at 06:01.b will still appear at
+	  06:01.b to the frontend). This is similar to how Xen 2.0.x exposed
+	  PCI devices to its driver domains. This may be required for drivers
+	  which depend on finding their hardward in certain bus/slot
+	  locations.
+
+config XEN_PCIDEV_BACKEND_SLOT
+	bool "Slot"
+	---help---
+	  This PCI Backend hides the true PCI topology and makes the frontend
+	  think there is a single PCI bus with only the exported devices on it.
+	  Contrary to the virtual PCI backend, a function becomes a new slot.
+	  For example, a device at 03:05.2 will be re-assigned to 00:00.0. A
+	  second device at 02:1a.1 will be re-assigned to 00:01.0.
+
+endchoice
+
+config XEN_PCIDEV_BE_DEBUG
+	bool "PCI Backend Debugging"
+	depends on XEN_PCIDEV_BACKEND
+	default n
+
+config XEN_TPMDEV_BACKEND
+	tristate "TPM-device backend driver"
+        depends on XEN_BACKEND
+	default n
+	help
+	  The TPM-device backend driver
+
+config XEN_BLKDEV_FRONTEND
+	tristate "Block-device frontend driver"
+	depends on XEN
+	default y
+	help
+	  The block-device frontend driver allows the kernel to access block
+	  devices mounted within another guest OS. Unless you are building a
+	  dedicated device-driver domain, or your master control domain
+	  (domain 0), then you almost certainly want to say Y here.
+
+config XEN_NETDEV_FRONTEND
+	tristate "Network-device frontend driver"
+	depends on XEN && NET
+	default y
+	help
+	  The network-device frontend driver allows the kernel to access
+	  network interfaces within another guest OS. Unless you are building a
+	  dedicated device-driver domain, or your master control domain
+	  (domain 0), then you almost certainly want to say Y here.
+
+config XEN_FRAMEBUFFER
+	tristate "Framebuffer-device frontend driver"
+	depends on XEN && FB
+	select FB_CFB_FILLRECT
+	select FB_CFB_COPYAREA
+	select FB_CFB_IMAGEBLIT
+	default y
+	help
+	  The framebuffer-device frontend drivers allows the kernel to create a
+	  virtual framebuffer.  This framebuffer can be viewed in another
+	  domain.  Unless this domain has access to a real video card, you
+	  probably want to say Y here.
+
+config XEN_KEYBOARD
+	tristate "Keyboard-device frontend driver"
+	depends on XEN && XEN_FRAMEBUFFER && INPUT
+	default y
+	help
+	  The keyboard-device frontend driver allows the kernel to create a
+	  virtual keyboard.  This keyboard can then be driven by another
+	  domain.  If you've said Y to CONFIG_XEN_FRAMEBUFFER, you probably
+	  want to say Y here.
+
+config XEN_SCRUB_PAGES
+	bool "Scrub memory before freeing it to Xen"
+	default y
+	help
+	  Erase memory contents before freeing it back to Xen's global
+	  pool. This ensures that any secrets contained within that
+	  memory (e.g., private keys) cannot be found by other guests that
+	  may be running on the machine. Most people will want to say Y here.
+	  If security is not a concern then you may increase performance by
+	  saying N.
+
+config XEN_DISABLE_SERIAL
+	bool "Disable serial port drivers"
+	default y
+	help
+	  Disable serial port drivers, allowing the Xen console driver
+	  to provide a serial console at ttyS0.
+
+config XEN_SYSFS
+	tristate "Export Xen attributes in sysfs"
+	depends on SYSFS
+	default y
+	help
+	  Xen hypervisor attributes will show up under /sys/hypervisor/.
+
+choice
+	prompt "Xen version compatibility"
+	default XEN_COMPAT_030002_AND_LATER
+
+	config XEN_COMPAT_030002_AND_LATER
+		bool "3.0.2 and later"
+
+	config XEN_COMPAT_030004_AND_LATER
+		bool "3.0.4 and later"
+
+	config XEN_COMPAT_LATEST_ONLY
+		bool "no compatibility code"
+
+endchoice
+
+config XEN_COMPAT
+	hex
+	default 0xffffff if XEN_COMPAT_LATEST_ONLY
+	default 0x030004 if XEN_COMPAT_030004_AND_LATER
+	default 0x030002 if XEN_COMPAT_030002_AND_LATER
+	default 0
+
+endmenu
+
+config HAVE_IRQ_IGNORE_UNHANDLED
+	bool
+	default y
+
+config NO_IDLE_HZ
+	bool
+	default y
+
+config XEN_SMPBOOT
+	bool
+	default y
+	depends on SMP
+
+endif
Index: head-2007-09-25/fs/Kconfig
===================================================================
--- head-2007-09-25.orig/fs/Kconfig	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/fs/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -1014,6 +1014,7 @@ config TMPFS_POSIX_ACL
 config HUGETLBFS
 	bool "HugeTLB file system support"
 	depends on X86 || IA64 || PPC64 || SPARC64 || SUPERH || BROKEN
+	depends on !XEN
 	help
 	  hugetlbfs is a filesystem backing for HugeTLB pages, based on
 	  ramfs. For architectures that support it, say Y here and read
Index: head-2007-09-25/kernel/Kconfig.preempt
===================================================================
--- head-2007-09-25.orig/kernel/Kconfig.preempt	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/kernel/Kconfig.preempt	2007-09-25 14:34:51.000000000 +0200
@@ -35,6 +35,7 @@ config PREEMPT_VOLUNTARY
 
 config PREEMPT
 	bool "Preemptible Kernel (Low-Latency Desktop)"
+	depends on !XEN
 	help
 	  This option reduces the latency of the kernel by making
 	  all kernel code (that is not executing in a critical section)
Index: head-2007-09-25/mm/Kconfig
===================================================================
--- head-2007-09-25.orig/mm/Kconfig	2007-09-25 14:22:36.000000000 +0200
+++ head-2007-09-25/mm/Kconfig	2007-09-25 14:34:51.000000000 +0200
@@ -132,11 +132,14 @@ config MEMORY_HOTPLUG_SPARSE
 # Default to 4 for wider testing, though 8 might be more appropriate.
 # ARM's adjust_pte (unused if VIPT) depends on mm-wide page_table_lock.
 # PA-RISC 7xxx's spinlock_t would enlarge struct page from 32 to 44 bytes.
+# XEN on x86 architecture uses the mapping field on pagetable pages to store a
+# pointer to the destructor. This conflicts with pte_lock_deinit().
 #
 config SPLIT_PTLOCK_CPUS
 	int
 	default "4096" if ARM && !CPU_CACHE_VIPT
 	default "4096" if PARISC && !PA20
+	default "4096" if X86_XEN || X86_64_XEN
 	default "4"
 
 #
