# HG changeset 95 patch
# User kfraser@localhost.localdomain
# Date 1184000854 -3600
# Node ID 21d5238ee2ec892825cc8905cc6ffa08b021fc88
# Parent  d36fd1c5db16e4531f78889a094fe0aeaa139995
Subject: privcmd: Take write lock on mm semaphore when calling
*remap_pfn_range(), as these function mess with fields in the vma
structure.
Signed-off-by: Christian Ehrhardt <ehrhardt@linux.vnet.ibm.com>
Signed-off-by: Hollis Blanchard <hollisb@us.ibm.com>

Acked-by: jbeulich@novell.com

---
 drivers/xen/privcmd/privcmd.c |   12 ++++++------
 1 file changed, 6 insertions(+), 6 deletions(-)

--- a/drivers/xen/privcmd/privcmd.c	2007-08-27 14:01:25.000000000 -0400
+++ b/drivers/xen/privcmd/privcmd.c	2007-08-27 14:02:03.000000000 -0400
@@ -111,7 +111,7 @@ static int privcmd_ioctl(struct inode *i
 		if (copy_from_user(&msg, p, sizeof(msg)))
 			return -EFAULT;
 
-		down_read(&mm->mmap_sem);
+		down_write(&mm->mmap_sem);
 
 		vma = find_vma(mm, msg.va);
 		rc = -EINVAL;
@@ -153,7 +153,7 @@ static int privcmd_ioctl(struct inode *i
 		rc = 0;
 
 	mmap_out:
-		up_read(&mm->mmap_sem);
+		up_write(&mm->mmap_sem);
 		ret = rc;
 	}
 	break;
@@ -176,14 +176,14 @@ static int privcmd_ioctl(struct inode *i
 		if ((m.num <= 0) || (nr_pages > (LONG_MAX >> PAGE_SHIFT)))
 			return -EINVAL;
 
-		down_read(&mm->mmap_sem);
+		down_write(&mm->mmap_sem);
 
 		vma = find_vma(mm, m.addr);
 		if (!vma ||
 		    (m.addr != vma->vm_start) ||
 		    ((m.addr + (nr_pages << PAGE_SHIFT)) != vma->vm_end) ||
 		    !privcmd_enforce_singleshot_mapping(vma)) {
-			up_read(&mm->mmap_sem);
+			up_write(&mm->mmap_sem);
 			return -EINVAL;
 		}
 
@@ -191,7 +191,7 @@ static int privcmd_ioctl(struct inode *i
 		addr = m.addr;
 		for (i = 0; i < nr_pages; i++, addr += PAGE_SIZE, p++) {
 			if (get_user(mfn, p)) {
-				up_read(&mm->mmap_sem);
+				up_write(&mm->mmap_sem);
 				return -EFAULT;
 			}
 
@@ -202,7 +202,7 @@ static int privcmd_ioctl(struct inode *i
 				put_user(0xF0000000 | mfn, p);
 		}
 
-		up_read(&mm->mmap_sem);
+		up_write(&mm->mmap_sem);
 		ret = 0;
 	}
 	break;
