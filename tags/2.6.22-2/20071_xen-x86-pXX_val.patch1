From: jbeulich@novell.com
Subject: consolidate pte_val/p[mug]d_val replacements
Patch-mainline: obsolete

- replace incomplete pXX_val_ma() set with complete __pXX_val() set
- use __pXX_val() instead of pXX_val() when only flags are accessed or
  the frame number is only compared against zero

Index: 10.3-2007-10-22/arch/i386/mm/hypervisor.c
===================================================================
--- 10.3-2007-10-22.orig/arch/i386/mm/hypervisor.c	2007-10-22 14:00:22.000000000 +0200
+++ 10.3-2007-10-22/arch/i386/mm/hypervisor.c	2007-10-22 14:02:29.000000000 +0200
@@ -44,17 +44,6 @@
 #include <linux/highmem.h>
 #include <asm/tlbflush.h>
 
-#ifdef CONFIG_X86_64
-#define pmd_val_ma(v) (v).pmd
-#else
-#ifdef CONFIG_X86_PAE
-# define pmd_val_ma(v) ((v).pmd)
-# define pud_val_ma(v) ((v).pgd.pgd)
-#else
-# define pmd_val_ma(v) ((v).pud.pgd.pgd)
-#endif
-#endif
-
 void xen_l1_entry_update(pte_t *ptr, pte_t val)
 {
 	mmu_update_t u;
@@ -64,7 +53,7 @@ void xen_l1_entry_update(pte_t *ptr, pte
 #else
 	u.ptr = virt_to_machine(ptr);
 #endif
-	u.val = pte_val_ma(val);
+	u.val = __pte_val(val);
 	BUG_ON(HYPERVISOR_mmu_update(&u, 1, NULL, DOMID_SELF) < 0);
 }
 
@@ -72,34 +61,26 @@ void xen_l2_entry_update(pmd_t *ptr, pmd
 {
 	mmu_update_t u;
 	u.ptr = virt_to_machine(ptr);
-	u.val = pmd_val_ma(val);
+	u.val = __pmd_val(val);
 	BUG_ON(HYPERVISOR_mmu_update(&u, 1, NULL, DOMID_SELF) < 0);
 }
 
-#ifdef CONFIG_X86_PAE
+#if defined(CONFIG_X86_PAE) || defined(CONFIG_X86_64)
 void xen_l3_entry_update(pud_t *ptr, pud_t val)
 {
 	mmu_update_t u;
 	u.ptr = virt_to_machine(ptr);
-	u.val = pud_val_ma(val);
+	u.val = __pud_val(val);
 	BUG_ON(HYPERVISOR_mmu_update(&u, 1, NULL, DOMID_SELF) < 0);
 }
 #endif
 
 #ifdef CONFIG_X86_64
-void xen_l3_entry_update(pud_t *ptr, pud_t val)
-{
-	mmu_update_t u;
-	u.ptr = virt_to_machine(ptr);
-	u.val = val.pud;
-	BUG_ON(HYPERVISOR_mmu_update(&u, 1, NULL, DOMID_SELF) < 0);
-}
-
 void xen_l4_entry_update(pgd_t *ptr, pgd_t val)
 {
 	mmu_update_t u;
 	u.ptr = virt_to_machine(ptr);
-	u.val = val.pgd;
+	u.val = __pgd_val(val);
 	BUG_ON(HYPERVISOR_mmu_update(&u, 1, NULL, DOMID_SELF) < 0);
 }
 #endif /* CONFIG_X86_64 */
Index: 10.3-2007-10-22/arch/i386/mm/ioremap-xen.c
===================================================================
--- 10.3-2007-10-22.orig/arch/i386/mm/ioremap-xen.c	2007-10-22 13:58:57.000000000 +0200
+++ 10.3-2007-10-22/arch/i386/mm/ioremap-xen.c	2007-10-22 14:01:59.000000000 +0200
@@ -77,7 +77,7 @@ static int __direct_remap_pfn_range(stru
 		 * Fill in the machine address: PTE ptr is done later by
 		 * __direct_remap_area_pages(). 
 		 */
-		v->val = pte_val_ma(pfn_pte_ma(mfn, prot));
+		v->val = __pte_val(pfn_pte_ma(mfn, prot));
 
 		mfn++;
 		address += PAGE_SIZE; 
Index: 10.3-2007-10-22/include/asm-i386/mach-xen/asm/page.h
===================================================================
--- 10.3-2007-10-22.orig/include/asm-i386/mach-xen/asm/page.h	2007-10-22 13:58:57.000000000 +0200
+++ 10.3-2007-10-22/include/asm-i386/mach-xen/asm/page.h	2007-10-22 14:01:59.000000000 +0200
@@ -76,17 +76,21 @@ typedef struct { unsigned long long pgpr
 #define pgprot_val(x)	((x).pgprot)
 #include <asm/maddr.h>
 
+#define __pgd_val(x) ((x).pgd)
 static inline unsigned long long xen_pgd_val(pgd_t pgd)
 {
-	unsigned long long ret = pgd.pgd;
+	unsigned long long ret = __pgd_val(pgd);
 	if (ret & _PAGE_PRESENT)
 		ret = pte_machine_to_phys(ret);
 	return ret;
 }
 
+#define __pud_val(x) __pgd_val((x).pgd)
+
+#define __pmd_val(x) ((x).pmd)
 static inline unsigned long long xen_pmd_val(pmd_t pmd)
 {
-	unsigned long long ret = pmd.pmd;
+	unsigned long long ret = __pmd_val(pmd);
 #if CONFIG_XEN_COMPAT <= 0x030002
 	if (ret)
 		ret = pte_machine_to_phys(ret) | _PAGE_PRESENT;
@@ -97,13 +101,13 @@ static inline unsigned long long xen_pmd
 	return ret;
 }
 
-static inline unsigned long long pte_val_ma(pte_t pte)
+static inline unsigned long long __pte_val(pte_t pte)
 {
 	return ((unsigned long long)pte.pte_high << 32) | pte.pte_low;
 }
 static inline unsigned long long xen_pte_val(pte_t pte)
 {
-	unsigned long long ret = pte_val_ma(pte);
+	unsigned long long ret = __pte_val(pte);
 	if (pte.pte_low & _PAGE_PRESENT)
 		ret = pte_machine_to_phys(ret);
 	return ret;
@@ -143,9 +147,10 @@ typedef struct { unsigned long pgprot; }
 #define boot_pte_t pte_t /* or would you rather have a typedef */
 #include <asm/maddr.h>
 
+#define __pgd_val(x) ((x).pgd)
 static inline unsigned long xen_pgd_val(pgd_t pgd)
 {
-	unsigned long ret = pgd.pgd;
+	unsigned long ret = __pgd_val(pgd);
 #if CONFIG_XEN_COMPAT <= 0x030002
 	if (ret)
 		ret = machine_to_phys(ret) | _PAGE_PRESENT;
@@ -156,13 +161,16 @@ static inline unsigned long xen_pgd_val(
 	return ret;
 }
 
-static inline unsigned long pte_val_ma(pte_t pte)
+#define __pud_val(x) __pgd_val((x).pgd)
+#define __pmd_val(x) __pud_val((x).pud)
+
+static inline unsigned long __pte_val(pte_t pte)
 {
 	return pte.pte_low;
 }
 static inline unsigned long xen_pte_val(pte_t pte)
 {
-	unsigned long ret = pte_val_ma(pte);
+	unsigned long ret = __pte_val(pte);
 	if (ret & _PAGE_PRESENT)
 		ret = machine_to_phys(ret);
 	return ret;
Index: 10.3-2007-10-22/include/asm-i386/mach-xen/asm/pgtable.h
===================================================================
--- 10.3-2007-10-22.orig/include/asm-i386/mach-xen/asm/pgtable.h	2007-10-22 14:09:14.000000000 +0200
+++ 10.3-2007-10-22/include/asm-i386/mach-xen/asm/pgtable.h	2007-10-22 14:09:48.000000000 +0200
@@ -210,15 +210,16 @@ extern unsigned long pg0[];
 #define pte_present(x)	((x).pte_low & (_PAGE_PRESENT | _PAGE_PROTNONE))
 
 /* To avoid harmful races, pmd_none(x) should check only the lower when PAE */
-#define pmd_none(x)	(!(unsigned long)pmd_val(x))
+#define pmd_none(x)	(!(unsigned long)__pmd_val(x))
 #if CONFIG_XEN_COMPAT <= 0x030002
 /* pmd_present doesn't just test the _PAGE_PRESENT bit since wr.p.t.
    can temporarily clear it. */
-#define pmd_present(x)	(pmd_val(x))
+#define pmd_present(x)	(__pmd_val(x))
+#define pmd_bad(x)	((__pmd_val(x) & (~PAGE_MASK & ~_PAGE_USER & ~_PAGE_PRESENT)) != (_KERNPG_TABLE & ~_PAGE_PRESENT))
 #else
-#define pmd_present(x)	(pmd_val(x) & _PAGE_PRESENT)
+#define pmd_present(x)	(__pmd_val(x) & _PAGE_PRESENT)
+#define pmd_bad(x)	((__pmd_val(x) & (~PAGE_MASK & ~_PAGE_USER)) != _KERNPG_TABLE)
 #endif
-#define pmd_bad(x)	((pmd_val(x) & (~PAGE_MASK & ~_PAGE_USER & ~_PAGE_PRESENT)) != (_KERNPG_TABLE & ~_PAGE_PRESENT))
 
 
 #define pages_to_mb(x) ((x) >> (20-PAGE_SHIFT))
@@ -442,7 +443,7 @@ static inline pte_t pte_modify(pte_t pte
 }
 
 #define pmd_large(pmd) \
-((pmd_val(pmd) & (_PAGE_PSE|_PAGE_PRESENT)) == (_PAGE_PSE|_PAGE_PRESENT))
+((__pmd_val(pmd) & (_PAGE_PSE|_PAGE_PRESENT)) == (_PAGE_PSE|_PAGE_PRESENT))
 
 /*
  * the pgd page can be thought of an array like this: pgd_t[PTRS_PER_PGD]
Index: 10.3-2007-10-22/include/asm-i386/mach-xen/asm/pgtable-2level.h
===================================================================
--- 10.3-2007-10-22.orig/include/asm-i386/mach-xen/asm/pgtable-2level.h	2007-10-22 13:58:57.000000000 +0200
+++ 10.3-2007-10-22/include/asm-i386/mach-xen/asm/pgtable-2level.h	2007-10-22 14:01:59.000000000 +0200
@@ -2,9 +2,11 @@
 #define _I386_PGTABLE_2LEVEL_H
 
 #define pte_ERROR(e) \
-	printk("%s:%d: bad pte %08lx.\n", __FILE__, __LINE__, (e).pte_low)
+	printk("%s:%d: bad pte %08lx (pfn %05lx).\n", __FILE__, __LINE__, \
+	       __pte_val(e), pte_pfn(e))
 #define pgd_ERROR(e) \
-	printk("%s:%d: bad pgd %08lx.\n", __FILE__, __LINE__, pgd_val(e))
+	printk("%s:%d: bad pgd %08lx (pfn %05lx).\n", __FILE__, __LINE__, \
+	       __pgd_val(e), pgd_val(e) >> PAGE_SHIFT)
 
 /*
  * Certain architectures need to do special things when PTEs
Index: 10.3-2007-10-22/include/asm-i386/mach-xen/asm/pgtable-3level.h
===================================================================
--- 10.3-2007-10-22.orig/include/asm-i386/mach-xen/asm/pgtable-3level.h	2007-10-22 13:58:57.000000000 +0200
+++ 10.3-2007-10-22/include/asm-i386/mach-xen/asm/pgtable-3level.h	2007-10-22 14:01:59.000000000 +0200
@@ -9,11 +9,14 @@
  */
 
 #define pte_ERROR(e) \
-	printk("%s:%d: bad pte %p(%08lx%08lx).\n", __FILE__, __LINE__, &(e), (e).pte_high, (e).pte_low)
+	printk("%s:%d: bad pte %p(%016Lx pfn %08lx).\n", __FILE__, __LINE__, \
+	       &(e), __pte_val(e), pte_pfn(e))
 #define pmd_ERROR(e) \
-	printk("%s:%d: bad pmd %p(%016Lx).\n", __FILE__, __LINE__, &(e), pmd_val(e))
+	printk("%s:%d: bad pmd %p(%016Lx pfn %08Lx).\n", __FILE__, __LINE__, \
+	       &(e), __pmd_val(e), (pmd_val(e) & PTE_MASK) >> PAGE_SHIFT)
 #define pgd_ERROR(e) \
-	printk("%s:%d: bad pgd %p(%016Lx).\n", __FILE__, __LINE__, &(e), pgd_val(e))
+	printk("%s:%d: bad pgd %p(%016Lx pfn %08Lx).\n", __FILE__, __LINE__, \
+	       &(e), __pgd_val(e), (pgd_val(e) & PTE_MASK) >> PAGE_SHIFT)
 
 #define pud_none(pud)				0
 #define pud_bad(pud)				0
@@ -24,7 +27,7 @@
  */
 static inline int pte_x(pte_t pte)
 {
-	return !(pte_val(pte) & _PAGE_NX);
+	return !(__pte_val(pte) & _PAGE_NX);
 }
 
 /*
@@ -66,7 +69,7 @@ static inline void xen_set_pte_at(struct
 
 static inline void xen_set_pte_atomic(pte_t *ptep, pte_t pte)
 {
-	set_64bit((unsigned long long *)(ptep),pte_val_ma(pte));
+	set_64bit((unsigned long long *)(ptep),__pte_val(pte));
 }
 static inline void xen_set_pmd(pmd_t *pmdp, pmd_t pmd)
 {
@@ -127,7 +130,7 @@ static inline void pud_clear (pud_t * pu
 #ifdef CONFIG_SMP
 static inline pte_t xen_ptep_get_and_clear(pte_t *ptep, pte_t res)
 {
-	uint64_t val = pte_val_ma(res);
+	uint64_t val = __pte_val(res);
 	if (__cmpxchg64(ptep, val, 0) != val) {
 		/* xchg acts as a barrier before the setting of the high bits */
 		res.pte_low = xchg(&ptep->pte_low, 0);
Index: 10.3-2007-10-22/include/asm-x86_64/mach-xen/asm/page.h
===================================================================
--- 10.3-2007-10-22.orig/include/asm-x86_64/mach-xen/asm/page.h	2007-10-22 13:58:57.000000000 +0200
+++ 10.3-2007-10-22/include/asm-x86_64/mach-xen/asm/page.h	2007-10-22 14:01:59.000000000 +0200
@@ -85,14 +85,15 @@ typedef struct { unsigned long pgd; } pg
 
 typedef struct { unsigned long pgprot; } pgprot_t;
 
-#define pte_val(x)	(((x).pte & _PAGE_PRESENT) ? \
-			 pte_machine_to_phys((x).pte) : \
-			 (x).pte)
-#define pte_val_ma(x)	((x).pte)
+#define __pte_val(x) ((x).pte)
+#define pte_val(x) ((__pte_val(x) & _PAGE_PRESENT) ? \
+                    pte_machine_to_phys(__pte_val(x)) : \
+                    __pte_val(x))
 
+#define __pmd_val(x) ((x).pmd)
 static inline unsigned long pmd_val(pmd_t x)
 {
-	unsigned long ret = x.pmd;
+	unsigned long ret = __pmd_val(x);
 #if CONFIG_XEN_COMPAT <= 0x030002
 	if (ret) ret = pte_machine_to_phys(ret) | _PAGE_PRESENT;
 #else
@@ -101,16 +102,18 @@ static inline unsigned long pmd_val(pmd_
 	return ret;
 }
 
+#define __pud_val(x) ((x).pud)
 static inline unsigned long pud_val(pud_t x)
 {
-	unsigned long ret = x.pud;
+	unsigned long ret = __pud_val(x);
 	if (ret & _PAGE_PRESENT) ret = pte_machine_to_phys(ret);
 	return ret;
 }
 
+#define __pgd_val(x) ((x).pgd)
 static inline unsigned long pgd_val(pgd_t x)
 {
-	unsigned long ret = x.pgd;
+	unsigned long ret = __pgd_val(x);
 	if (ret & _PAGE_PRESENT) ret = pte_machine_to_phys(ret);
 	return ret;
 }
Index: 10.3-2007-10-22/include/asm-x86_64/mach-xen/asm/pgtable.h
===================================================================
--- 10.3-2007-10-22.orig/include/asm-x86_64/mach-xen/asm/pgtable.h	2007-10-22 14:00:10.000000000 +0200
+++ 10.3-2007-10-22/include/asm-x86_64/mach-xen/asm/pgtable.h	2007-10-22 14:01:59.000000000 +0200
@@ -82,16 +82,20 @@ extern unsigned long empty_zero_page[PAG
 #ifndef __ASSEMBLY__
 
 #define pte_ERROR(e) \
-	printk("%s:%d: bad pte %p(%016lx).\n", __FILE__, __LINE__, &(e), pte_val(e))
+	printk("%s:%d: bad pte %p(%016lx pfn %010lx).\n", __FILE__, __LINE__, \
+	       &(e), __pte_val(e), pte_pfn(e))
 #define pmd_ERROR(e) \
-	printk("%s:%d: bad pmd %p(%016lx).\n", __FILE__, __LINE__, &(e), pmd_val(e))
+	printk("%s:%d: bad pmd %p(%016lx pfn %010lx).\n", __FILE__, __LINE__, \
+	       &(e), __pmd_val(e), pmd_pfn(e))
 #define pud_ERROR(e) \
-	printk("%s:%d: bad pud %p(%016lx).\n", __FILE__, __LINE__, &(e), pud_val(e))
+	printk("%s:%d: bad pud %p(%016lx pfn %010lx).\n", __FILE__, __LINE__, \
+	       &(e), __pud_val(e), (pud_val(e) & __PHYSICAL_MASK) >> PAGE_SHIFT)
 #define pgd_ERROR(e) \
-	printk("%s:%d: bad pgd %p(%016lx).\n", __FILE__, __LINE__, &(e), pgd_val(e))
+	printk("%s:%d: bad pgd %p(%016lx pfn %010lx).\n", __FILE__, __LINE__, \
+	       &(e), __pgd_val(e), (pgd_val(e) & __PHYSICAL_MASK) >> PAGE_SHIFT)
 
-#define pgd_none(x)	(!pgd_val(x))
-#define pud_none(x)	(!pud_val(x))
+#define pgd_none(x)	(!__pgd_val(x))
+#define pud_none(x)	(!__pud_val(x))
 
 static inline void set_pte(pte_t *dst, pte_t val)
 {
@@ -236,17 +240,17 @@ extern unsigned int __kernel_page_user;
 
 static inline unsigned long pgd_bad(pgd_t pgd)
 {
-	return pgd_val(pgd) & ~(PTE_MASK | _KERNPG_TABLE | _PAGE_USER);
+	return __pgd_val(pgd) & ~(PTE_MASK | _KERNPG_TABLE | _PAGE_USER);
 }
 
 static inline unsigned long pud_bad(pud_t pud)
 {
-	return pud_val(pud) & ~(PTE_MASK | _KERNPG_TABLE | _PAGE_USER);
+	return __pud_val(pud) & ~(PTE_MASK | _KERNPG_TABLE | _PAGE_USER);
 }
 
 static inline unsigned long pmd_bad(pmd_t pmd)
 {
-	return pmd_val(pmd) & ~(PTE_MASK | _KERNPG_TABLE | _PAGE_USER);
+	return __pmd_val(pmd) & ~(PTE_MASK | _KERNPG_TABLE | _PAGE_USER);
 }
 
 #define set_pte_at(_mm,addr,ptep,pteval) do {				\
@@ -320,8 +324,6 @@ static inline pte_t ptep_get_and_clear_f
  * The following only work if pte_present() is true.
  * Undefined behaviour if not..
  */
-#define __pte_val(x)	((x).pte)
-
 #define __LARGE_PTE (_PAGE_PSE|_PAGE_PRESENT)
 static inline int pte_user(pte_t pte)		{ return __pte_val(pte) & _PAGE_USER; }
 static inline int pte_read(pte_t pte)		{ return __pte_val(pte) & _PAGE_USER; }
@@ -372,7 +374,7 @@ static inline void ptep_set_wrprotect(st
 #define pgprot_noncached(prot)	(__pgprot(pgprot_val(prot) | _PAGE_PCD | _PAGE_PWT))
 
 static inline int pmd_large(pmd_t pte) { 
-	return (pmd_val(pte) & __LARGE_PTE) == __LARGE_PTE; 
+	return (__pmd_val(pte) & __LARGE_PTE) == __LARGE_PTE;
 } 	
 
 
@@ -390,7 +392,7 @@ static inline int pmd_large(pmd_t pte) {
 #define pgd_index(address) (((address) >> PGDIR_SHIFT) & (PTRS_PER_PGD-1))
 #define pgd_offset(mm, addr) ((mm)->pgd + pgd_index(addr))
 #define pgd_offset_k(address) (init_level4_pgt + pgd_index(address))
-#define pgd_present(pgd) (pgd_val(pgd) & _PAGE_PRESENT)
+#define pgd_present(pgd) (__pgd_val(pgd) & _PAGE_PRESENT)
 #define mk_kernel_pgd(address) __pgd((address) | _KERNPG_TABLE)
 
 /* PUD - Level3 access */
@@ -399,7 +401,7 @@ static inline int pmd_large(pmd_t pte) {
 #define pud_page(pud)		(pfn_to_page(pud_val(pud) >> PAGE_SHIFT))
 #define pud_index(address) (((address) >> PUD_SHIFT) & (PTRS_PER_PUD-1))
 #define pud_offset(pgd, address) ((pud_t *) pgd_page_vaddr(*(pgd)) + pud_index(address))
-#define pud_present(pud) (pud_val(pud) & _PAGE_PRESENT)
+#define pud_present(pud) (__pud_val(pud) & _PAGE_PRESENT)
 
 /* PMD  - Level 2 access */
 #define pmd_page_vaddr(pmd) ((unsigned long) __va(pmd_val(pmd) & PTE_MASK))
@@ -408,19 +410,19 @@ static inline int pmd_large(pmd_t pte) {
 #define pmd_index(address) (((address) >> PMD_SHIFT) & (PTRS_PER_PMD-1))
 #define pmd_offset(dir, address) ((pmd_t *) pud_page_vaddr(*(dir)) + \
                                   pmd_index(address))
-#define pmd_none(x)	(!pmd_val(x))
+#define pmd_none(x)	(!__pmd_val(x))
 #if CONFIG_XEN_COMPAT <= 0x030002
 /* pmd_present doesn't just test the _PAGE_PRESENT bit since wr.p.t.
    can temporarily clear it. */
-#define pmd_present(x)	(pmd_val(x))
+#define pmd_present(x)	(__pmd_val(x))
 #else
-#define pmd_present(x)	(pmd_val(x) & _PAGE_PRESENT)
+#define pmd_present(x)	(__pmd_val(x) & _PAGE_PRESENT)
 #endif
 #define pmd_clear(xp)	do { set_pmd(xp, __pmd(0)); } while (0)
 #define pfn_pmd(nr,prot) (__pmd(((nr) << PAGE_SHIFT) | pgprot_val(prot)))
 #define pmd_pfn(x)  ((pmd_val(x) & __PHYSICAL_MASK) >> PAGE_SHIFT)
 
-#define pte_to_pgoff(pte) ((pte_val(pte) & PHYSICAL_PAGE_MASK) >> PAGE_SHIFT)
+#define pte_to_pgoff(pte) ((__pte_val(pte) & PHYSICAL_PAGE_MASK) >> PAGE_SHIFT)
 #define pgoff_to_pte(off) ((pte_t) { ((off) << PAGE_SHIFT) | _PAGE_FILE })
 #define PTE_FILE_MAX_BITS __PHYSICAL_MASK_SHIFT
 
@@ -428,7 +430,7 @@ static inline int pmd_large(pmd_t pte) {
 
 /* page, protection -> pte */
 #define mk_pte(page, pgprot)	pfn_pte(page_to_pfn(page), (pgprot))
-#define mk_pte_huge(entry) (pte_val(entry) |= _PAGE_PRESENT | _PAGE_PSE)
+#define mk_pte_huge(entry) (__pte_val(entry) |= _PAGE_PRESENT | _PAGE_PSE)
  
 /* Change flags of a PTE */
 static inline pte_t pte_modify(pte_t pte, pgprot_t newprot)
