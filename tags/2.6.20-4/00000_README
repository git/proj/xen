Xen Patches README
------------------

These patches are intended to be stacked on top of genpatches-base.

Many of the patches included here are swiped from various sources which
use their own four digit patch numbering scheme, so we are stuck with five
digits to indiciate the source for easier tracking and re-syncing.

Numbering
---------

0xxxx	Gentoo, not related to Xen. (in case we pull something from extras)
1xxxx	XenSource, upstream Xen patch for 2.6.18
2xxxx	Redhat, we use their Xen patch for >=2.6.20
3xxxx	Debian, we use their security fixes for 2.6.18
4xxxx	Misc
5xxxx	Gentoo, Xen and other fixes for Redhat and/or Debian patches.

Patches
-------

20950_linux-2.6.20.14-xen-3.1.0.patch
    Main Xen patch

20xxx-?
    Various bug-fix patches from Redhat.

30037_amd64-zero-extend-32bit-ptrace-xen.patch
    [SECURITY] Zero extend all registers after ptrace in 32-bit entry path
    (Xen).
    See CVE-2007-4573

40001_i386-fix-xen_l1_entry_update-for-highptes.patch
    Fix for kernels compiled with CONFIG_HIGHPTE.
    Pulled from linux-2.6.18-xen.hg, changeset e79729740288.

50001_make-install.patch
    Handle make install in a semi-sane way that plays nice with
    split domU/dom0 kernels.

50002_always-enable-xen-genapic.patch
    Compile fix for non-SMP (UP) kernels. Since UP support is broken in
    upstream Xen I'm not sure if I trust it or not. :-P

50003_console-tty-fix.patch
    Steal tty1-63 as the upstream Xen release does so people don't get
    any supprises. Redhat switched to using the special Xen tty device.

50004_quirks-no-smp-fix.patch
    Another compile fix for non-SMP (UP) kernels.

50006_pgetable-build-fix.patch
    Fix a function re-definition error when PAE is not enabled.

50008_reenable-tls-warning.patch
    Issue only one big fat tls warning as upstream xen does.

50009_gentooify-tls-warning.patch
    Change tls warning instructions to apply directly to Gentoo.
