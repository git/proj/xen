From: ksrinivasan@novell.com
Subject: Don't allow ballooning down a domain below a reasonable limit.
References: 172482

Reasonable is hard to judge; we don't want to disallow small domains.
But the system needs a reasonable amount of memory to perform its
duties, set up tables, etc. If on the other hand, the admin is able
to set up and boot up correctly a very small domain, there's no point
in forcing it to be larger.
We end up with some kind of logarithmic function, approximated.

Memory changes are logged, so making domains too small should at least
result in a trace.

Signed-off-by: Kurt Garloff <garloff@suse.de>

Index: head-2007-10-08/drivers/xen/balloon/balloon.c
===================================================================
--- head-2007-10-08.orig/drivers/xen/balloon/balloon.c	2007-10-09 09:31:50.000000000 +0200
+++ head-2007-10-08/drivers/xen/balloon/balloon.c	2007-10-09 09:32:02.000000000 +0200
@@ -181,6 +181,38 @@ static unsigned long current_target(void
 	return target;
 }
 
+static unsigned long minimum_target(void)
+{
+	unsigned long min_pages;
+	unsigned long curr_pages = current_target();
+
+#define MB2PAGES(mb) ((mb) << (20 - PAGE_SHIFT))
+	/* Simple continuous piecewiese linear function:
+	 *  max MiB -> min MiB	gradient
+	 *       0	   0
+	 *      16	  16
+	 *      32	  24
+	 *     128	  72	(1/2)
+	 *     512 	 168	(1/4)
+	 *    2048	 360	(1/8)
+	 *    8192	 552	(1/32)
+	 *   32768	1320
+	 *  131072	4392
+	 */
+	if (max_pfn < MB2PAGES(128))
+		min_pages = MB2PAGES(8) + (max_pfn >> 1);
+	else if (max_pfn < MB2PAGES(512))
+		min_pages = MB2PAGES(40) + (max_pfn >> 2);
+	else if (max_pfn < MB2PAGES(2048))
+		min_pages = MB2PAGES(104) + (max_pfn >> 3);
+	else
+		min_pages = MB2PAGES(296) + (max_pfn >> 5);
+#undef MB2PAGES
+
+	/* Don't enforce growth */
+	return min_pages < curr_pages ? min_pages : curr_pages;
+}
+
 static int increase_reservation(unsigned long nr_pages)
 {
 	unsigned long  pfn, i, flags;
@@ -369,6 +401,17 @@ static void balloon_process(struct work_
 /* Resets the Xen limit, sets new target, and kicks off processing. */
 void balloon_set_new_target(unsigned long target)
 {
+	/* First make sure that we are not lowering the value below the
+	 * "minimum".
+	 */
+	unsigned long min_pages = minimum_target();
+
+	if (target < min_pages)
+		target = min_pages;
+
+	printk(KERN_INFO "Setting mem allocation to %lu kiB\n",
+	       PAGES2KB(target));
+
 	/* No need for lock. Not read-modify-write updates. */
 	bs.hard_limit   = ~0UL;
 	bs.target_pages = target;
