Subject: Fix IDE CD-drive PIO mode.
From: http://xenbits.xensource.com/xen-3.1-testing.hg (tip 15042)
Signed-off-by: Christian Limpach <Christian.Limpach@cl.cam.ac.uk>

CD drives in PIO mode don't work under Xen because of a change in Linux
between 2.6.12 and 2.6.16, as a result of the following thread:
http://lists.parisc-linux.org/pipermail/parisc-linux/2005-August/027197.html

The change breaks systems which have highmem and a swiotlb because the
ide-cd driver doesn't use the swiotlb, resulting in read/writes to/from
highmem pages in PIO mode not working any longer.  Xen kernels usually have
both highmem and a swiotlb.

Acked-by: Jan Beulich <jbeulich@novell.com>

---
 drivers/ide/ide-lib.c |    8 ++++----
 1 file changed, 4 insertions(+), 4 deletions(-)

--- a/drivers/ide/ide-lib.c	2007-08-27 12:09:26.000000000 -0400
+++ b/drivers/ide/ide-lib.c	2007-08-27 14:02:05.000000000 -0400
@@ -341,10 +341,10 @@ void ide_toggle_bounce(ide_drive_t *driv
 {
 	u64 addr = BLK_BOUNCE_HIGH;	/* dma64_addr_t */
 
-	if (!PCI_DMA_BUS_IS_PHYS) {
-		addr = BLK_BOUNCE_ANY;
-	} else if (on && drive->media == ide_disk) {
-		if (HWIF(drive)->pci_dev)
+	if (on && drive->media == ide_disk) {
+		if (!PCI_DMA_BUS_IS_PHYS)
+			addr = BLK_BOUNCE_ANY;
+		else if (HWIF(drive)->pci_dev)
 			addr = HWIF(drive)->pci_dev->dma_mask;
 	}
 
