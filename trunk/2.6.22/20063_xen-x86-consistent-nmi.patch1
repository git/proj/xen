From: jbeulich@novell.com
Subject: make i386 and x86 NMI code consistent, disable all APIC-related stuff
Patch-mainline: obsolete
References: 191115

Index: head-2007-09-03/arch/i386/kernel/cpu/Makefile
===================================================================
--- head-2007-09-03.orig/arch/i386/kernel/cpu/Makefile	2007-09-03 09:44:16.000000000 +0200
+++ head-2007-09-03/arch/i386/kernel/cpu/Makefile	2007-09-03 09:53:53.000000000 +0200
@@ -22,5 +22,7 @@ obj-$(CONFIG_X86_LOCAL_APIC) += perfctr-
 
 ifdef CONFIG_XEN
 include $(srctree)/scripts/Makefile.xen
+n-obj-xen := perfctr-watchdog.o
+obj-y := $(call filterxen, $(obj-y), $(n-obj-xen))
 obj-y := $(call cherrypickxen, $(obj-y), $(src))
 endif
Index: head-2007-09-03/arch/i386/kernel/nmi.c
===================================================================
--- head-2007-09-03.orig/arch/i386/kernel/nmi.c	2007-09-03 09:42:53.000000000 +0200
+++ head-2007-09-03/arch/i386/kernel/nmi.c	2007-09-03 09:53:53.000000000 +0200
@@ -30,7 +30,15 @@
 
 #include "mach_traps.h"
 
+#ifdef CONFIG_SYSCTL
 int unknown_nmi_panic;
+static int unknown_nmi_panic_callback(struct pt_regs *regs, int cpu);
+#endif
+
+extern void die_nmi(struct pt_regs *, const char *msg);
+
+#ifndef CONFIG_XEN
+
 int nmi_watchdog_enabled;
 
 static cpumask_t backtrace_mask = CPU_MASK_NONE;
@@ -48,9 +56,6 @@ static unsigned int nmi_hz = HZ;
 
 static DEFINE_PER_CPU(short, wd_enabled);
 
-/* local prototypes */
-static int unknown_nmi_panic_callback(struct pt_regs *regs, int cpu);
-
 static int endflag __initdata = 0;
 
 #ifdef CONFIG_SMP
@@ -315,8 +320,6 @@ void touch_nmi_watchdog (void)
 }
 EXPORT_SYMBOL(touch_nmi_watchdog);
 
-extern void die_nmi(struct pt_regs *, const char *msg);
-
 __kprobes int nmi_watchdog_tick(struct pt_regs * regs, unsigned reason)
 {
 
@@ -387,6 +390,8 @@ __kprobes int nmi_watchdog_tick(struct p
 	return rc;
 }
 
+#endif /* CONFIG_XEN */
+
 int do_nmi_callback(struct pt_regs * regs, int cpu)
 {
 #ifdef CONFIG_SYSCTL
@@ -408,6 +413,7 @@ static int unknown_nmi_panic_callback(st
 	return 0;
 }
 
+#ifndef CONFIG_XEN
 /*
  * proc handler for /proc/sys/kernel/nmi
  */
@@ -446,9 +452,11 @@ int proc_nmi_enabled(struct ctl_table *t
 	}
 	return 0;
 }
+#endif
 
 #endif
 
+#ifndef CONFIG_XEN
 void __trigger_all_cpu_backtrace(void)
 {
 	int i;
@@ -464,3 +472,4 @@ void __trigger_all_cpu_backtrace(void)
 
 EXPORT_SYMBOL(nmi_active);
 EXPORT_SYMBOL(nmi_watchdog);
+#endif
Index: head-2007-09-03/arch/i386/kernel/traps-xen.c
===================================================================
--- head-2007-09-03.orig/arch/i386/kernel/traps-xen.c	2007-09-03 09:53:48.000000000 +0200
+++ head-2007-09-03/arch/i386/kernel/traps-xen.c	2007-09-03 09:53:53.000000000 +0200
@@ -696,12 +696,14 @@ static __kprobes void default_do_nmi(str
 							== NOTIFY_STOP)
 			return;
 #ifdef CONFIG_X86_LOCAL_APIC
+#ifndef CONFIG_XEN
 		/*
 		 * Ok, so this is none of the documented NMI sources,
 		 * so it must be the NMI watchdog.
 		 */
 		if (nmi_watchdog_tick(regs, reason))
 			return;
+#endif
 		if (!do_nmi_callback(regs, smp_processor_id()))
 #endif
 			unknown_nmi_error(reason, regs);
Index: head-2007-09-03/arch/x86_64/kernel/Makefile
===================================================================
--- head-2007-09-03.orig/arch/x86_64/kernel/Makefile	2007-09-03 09:53:19.000000000 +0200
+++ head-2007-09-03/arch/x86_64/kernel/Makefile	2007-09-03 09:53:53.000000000 +0200
@@ -68,7 +68,7 @@ pci-dma-y			+= ../../i386/kernel/pci-dma
 microcode-$(subst m,y,$(CONFIG_MICROCODE))  := ../../i386/kernel/microcode-xen.o
 quirks-y			:= ../../i386/kernel/quirks-xen.o
 
-n-obj-xen := early-quirks.o i8259.o reboot.o i8237.o smpboot.o trampoline.o tsc.o tsc_sync.o
+n-obj-xen := early-quirks.o i8259.o reboot.o i8237.o perfctr-watchdog.o smpboot.o trampoline.o tsc.o tsc_sync.o
 
 include $(srctree)/scripts/Makefile.xen
 
Index: head-2007-09-03/arch/x86_64/kernel/nmi.c
===================================================================
--- head-2007-09-03.orig/arch/x86_64/kernel/nmi.c	2007-09-03 09:42:53.000000000 +0200
+++ head-2007-09-03/arch/x86_64/kernel/nmi.c	2007-09-03 09:53:53.000000000 +0200
@@ -28,10 +28,17 @@
 #include <asm/proto.h>
 #include <asm/mce.h>
 
+#ifdef CONFIG_SYSCTL
 int unknown_nmi_panic;
-int nmi_watchdog_enabled;
+static int unknown_nmi_panic_callback(struct pt_regs *regs, int cpu);
+#endif
+
 int panic_on_unrecovered_nmi;
 
+#ifndef CONFIG_XEN
+
+int nmi_watchdog_enabled;
+
 static cpumask_t backtrace_mask = CPU_MASK_NONE;
 
 /* nmi_active:
@@ -48,9 +55,6 @@ static unsigned int nmi_hz = HZ;
 
 static DEFINE_PER_CPU(short, wd_enabled);
 
-/* local prototypes */
-static int unknown_nmi_panic_callback(struct pt_regs *regs, int cpu);
-
 /* Run after command line and cpu_init init, but before all other checks */
 void nmi_watchdog_default(void)
 {
@@ -382,6 +386,8 @@ int __kprobes nmi_watchdog_tick(struct p
 	return rc;
 }
 
+#endif /* CONFIG_XEN */
+
 asmlinkage __kprobes void do_nmi(struct pt_regs * regs, long error_code)
 {
 	nmi_enter();
@@ -411,6 +417,7 @@ static int unknown_nmi_panic_callback(st
 	return 0;
 }
 
+#ifndef CONFIG_XEN
 /*
  * proc handler for /proc/sys/kernel/nmi
  */
@@ -445,9 +452,11 @@ int proc_nmi_enabled(struct ctl_table *t
 	}
 	return 0;
 }
+#endif
 
 #endif
 
+#ifndef CONFIG_XEN
 void __trigger_all_cpu_backtrace(void)
 {
 	int i;
@@ -464,3 +473,4 @@ void __trigger_all_cpu_backtrace(void)
 EXPORT_SYMBOL(nmi_active);
 EXPORT_SYMBOL(nmi_watchdog);
 EXPORT_SYMBOL(touch_nmi_watchdog);
+#endif /* CONFIG_XEN */
Index: head-2007-09-03/arch/x86_64/kernel/traps-xen.c
===================================================================
--- head-2007-09-03.orig/arch/x86_64/kernel/traps-xen.c	2007-09-03 09:53:45.000000000 +0200
+++ head-2007-09-03/arch/x86_64/kernel/traps-xen.c	2007-09-03 09:53:53.000000000 +0200
@@ -780,7 +780,7 @@ asmlinkage __kprobes void default_do_nmi
 		if (notify_die(DIE_NMI_IPI, "nmi_ipi", regs, reason, 2, SIGINT)
 								== NOTIFY_STOP)
 			return;
-#ifdef CONFIG_X86_LOCAL_APIC
+#if defined(CONFIG_X86_LOCAL_APIC) && !defined(CONFIG_XEN)
 		/*
 		 * Ok, so this is none of the documented NMI sources,
 		 * so it must be the NMI watchdog.
Index: head-2007-09-03/include/asm-i386/irq.h
===================================================================
--- head-2007-09-03.orig/include/asm-i386/irq.h	2007-09-03 09:42:53.000000000 +0200
+++ head-2007-09-03/include/asm-i386/irq.h	2007-09-03 09:53:53.000000000 +0200
@@ -20,7 +20,7 @@ static __inline__ int irq_canonicalize(i
 	return ((irq == 2) ? 9 : irq);
 }
 
-#ifdef CONFIG_X86_LOCAL_APIC
+#if defined(CONFIG_X86_LOCAL_APIC) && !defined(CONFIG_XEN)
 # define ARCH_HAS_NMI_WATCHDOG		/* See include/linux/nmi.h */
 #endif
 
Index: head-2007-09-03/include/asm-i386/nmi.h
===================================================================
--- head-2007-09-03.orig/include/asm-i386/nmi.h	2007-09-03 09:42:53.000000000 +0200
+++ head-2007-09-03/include/asm-i386/nmi.h	2007-09-03 09:53:53.000000000 +0200
@@ -7,8 +7,6 @@
 #include <linux/pm.h>
 #include <asm/irq.h>
 
-#ifdef ARCH_HAS_NMI_WATCHDOG
-
 /**
  * do_nmi_callback
  *
@@ -17,6 +15,8 @@
  */
 int do_nmi_callback(struct pt_regs *regs, int cpu);
 
+#ifdef ARCH_HAS_NMI_WATCHDOG
+
 extern int nmi_watchdog_enabled;
 extern int avail_to_resrv_perfctr_nmi_bit(unsigned int);
 extern int avail_to_resrv_perfctr_nmi(unsigned int);
@@ -43,13 +43,10 @@ struct ctl_table;
 struct file;
 extern int proc_nmi_enabled(struct ctl_table *, int , struct file *,
 			void __user *, size_t *, loff_t *);
-extern int unknown_nmi_panic;
 
 void __trigger_all_cpu_backtrace(void);
 #define trigger_all_cpu_backtrace() __trigger_all_cpu_backtrace()
 
-#endif
-
 void lapic_watchdog_stop(void);
 int lapic_watchdog_init(unsigned nmi_hz);
 int lapic_wd_event(unsigned nmi_hz);
@@ -58,4 +55,8 @@ int lapic_watchdog_ok(void);
 void disable_lapic_nmi_watchdog(void);
 void enable_lapic_nmi_watchdog(void);
 
+#endif
+
+extern int unknown_nmi_panic;
+
 #endif /* ASM_NMI_H */
Index: head-2007-09-03/include/asm-x86_64/mach-xen/asm/irq.h
===================================================================
--- head-2007-09-03.orig/include/asm-x86_64/mach-xen/asm/irq.h	2007-09-03 09:42:53.000000000 +0200
+++ head-2007-09-03/include/asm-x86_64/mach-xen/asm/irq.h	2007-09-03 09:53:53.000000000 +0200
@@ -20,7 +20,7 @@ static __inline__ int irq_canonicalize(i
 	return ((irq == 2) ? 9 : irq);
 }
 
-#ifdef CONFIG_X86_LOCAL_APIC
+#ifndef CONFIG_XEN
 #define ARCH_HAS_NMI_WATCHDOG		/* See include/linux/nmi.h */
 #endif
 
Index: head-2007-09-03/include/asm-x86_64/mach-xen/asm/nmi.h
===================================================================
--- head-2007-09-03.orig/include/asm-x86_64/mach-xen/asm/nmi.h	2007-09-03 09:53:30.000000000 +0200
+++ head-2007-09-03/include/asm-x86_64/mach-xen/asm/nmi.h	2007-09-03 09:53:53.000000000 +0200
@@ -59,6 +59,9 @@ static inline unsigned char get_nmi_reas
 
 extern int panic_on_timeout;
 extern int unknown_nmi_panic;
+
+#ifndef CONFIG_XEN
+
 extern int nmi_watchdog_enabled;
 
 extern int check_nmi_watchdog(void);
@@ -105,4 +108,6 @@ int lapic_watchdog_ok(void);
 void disable_lapic_nmi_watchdog(void);
 void enable_lapic_nmi_watchdog(void);
 
+#endif
+
 #endif /* ASM_NMI_H */
Index: head-2007-09-03/include/asm-x86_64/mach-xen/setup_arch_post.h
===================================================================
--- head-2007-09-03.orig/include/asm-x86_64/mach-xen/setup_arch_post.h	2007-09-03 09:42:53.000000000 +0200
+++ head-2007-09-03/include/asm-x86_64/mach-xen/setup_arch_post.h	2007-09-03 09:53:53.000000000 +0200
@@ -27,12 +27,10 @@ static void __init machine_specific_arch
 		.type = CALLBACKTYPE_syscall,
 		.address = (unsigned long)system_call,
 	};
-#ifdef CONFIG_X86_LOCAL_APIC
 	static struct callback_register __initdata nmi_cb = {
 		.type = CALLBACKTYPE_nmi,
 		.address = (unsigned long)nmi,
 	};
-#endif
 
 	ret = HYPERVISOR_callback_op(CALLBACKOP_register, &event);
 	if (ret == 0)
@@ -48,7 +46,6 @@ static void __init machine_specific_arch
 #endif
 	BUG_ON(ret);
 
-#ifdef CONFIG_X86_LOCAL_APIC
 	ret = HYPERVISOR_callback_op(CALLBACKOP_register, &nmi_cb);
 #if CONFIG_XEN_COMPAT <= 0x030002
 	if (ret == -ENOSYS) {
@@ -59,5 +56,4 @@ static void __init machine_specific_arch
 		HYPERVISOR_nmi_op(XENNMI_register_callback, &cb);
 	}
 #endif
-#endif
 }
Index: head-2007-09-03/kernel/sysctl.c
===================================================================
--- head-2007-09-03.orig/kernel/sysctl.c	2007-09-03 09:42:53.000000000 +0200
+++ head-2007-09-03/kernel/sysctl.c	2007-09-03 09:53:53.000000000 +0200
@@ -522,6 +522,7 @@ static ctl_table kern_table[] = {
 		.mode           = 0644,
 		.proc_handler   = &proc_dointvec,
 	},
+#ifndef CONFIG_XEN
 	{
 		.ctl_name       = KERN_NMI_WATCHDOG,
 		.procname       = "nmi_watchdog",
@@ -531,6 +532,7 @@ static ctl_table kern_table[] = {
 		.proc_handler   = &proc_nmi_enabled,
 	},
 #endif
+#endif
 #if defined(CONFIG_X86)
 	{
 		.ctl_name	= KERN_PANIC_ON_NMI,
